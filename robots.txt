User-agent: Yandex
Disallow: /author/
Disallow: /categories/
Disallow: /tags/
Disallow: /drafts/
Disallow: /images/
Disallow: /download/
Disallow: /*index*.html
Disallow: /theme/
Disallow: /*?*

Host: archive.kalnytskyi.com


User-agent: *
Disallow: /author/
Disallow: /categories/
Disallow: /tags/
Disallow: /drafts/
Disallow: /images/
Disallow: /download/
Disallow: /theme/
Disallow: /*index*.html
Disallow: /*?*
