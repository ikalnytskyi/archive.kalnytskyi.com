<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"><title>kalnitsky's way</title><link href="http://archive.kalnitsky.org/" rel="alternate"></link><link href="http://archive.kalnitsky.org//tags/algorithms/feed/index.xml" rel="self"></link><id>http://archive.kalnitsky.org/</id><updated>2011-05-01T00:00:00+03:00</updated><entry><title>Вызов некоторого метода у всех элементов контейнера в C++</title><link href="http://archive.kalnitsky.org/2011/05/01/how-to-call-method-for-all-elements/" rel="alternate"></link><updated>2011-05-01T00:00:00+03:00</updated><author><name>Igor Kalnitsky</name></author><id>tag:archive.kalnitsky.org,2011-05-01:2011/05/01/how-to-call-method-for-all-elements/</id><summary type="html">&lt;p&gt;Не раз замечаю, что люди абсолютно не знают STL. А зря, ведь
стандартная библиотека шаблонов предлагает широкий спектр ассортимента на
все случаи жизни. Многие и впрямь считают, что STL — это исключительно
контейнеры, что меня, признаться, пугает. Поэтому, в этом и последующем
постах, я хочу написать о некоторых возможностях и функциональностях STL, о
которых новички (я надеюсь, что &lt;strong&gt;только&lt;/strong&gt; новички) и не подозревают.&lt;/p&gt;
&lt;p&gt;Я сторонник обучения на примерах, поэтому, начну с кода. К примеру, есть
некий класс &lt;tt class="docutils literal"&gt;Foo&lt;/tt&gt;:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="k"&gt;class&lt;/span&gt; &lt;span class="nc"&gt;Foo&lt;/span&gt;
&lt;span class="p"&gt;{&lt;/span&gt;
&lt;span class="nl"&gt;public:&lt;/span&gt;
    &lt;span class="kt"&gt;void&lt;/span&gt; &lt;span class="n"&gt;doSomething&lt;/span&gt;&lt;span class="p"&gt;()&lt;/span&gt;&lt;span class="k"&gt;const&lt;/span&gt; &lt;span class="p"&gt;{&lt;/span&gt; &lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;cout&lt;/span&gt; &lt;span class="o"&gt;&amp;lt;&amp;lt;&lt;/span&gt; &lt;span class="s"&gt;&amp;quot;doSomething()&amp;quot;&lt;/span&gt;  &lt;span class="o"&gt;&amp;lt;&amp;lt;&lt;/span&gt; &lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;endl&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt; &lt;span class="p"&gt;}&lt;/span&gt;
&lt;span class="p"&gt;};&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Объекты этого класса находятся в некотором контейнере, допустим, векторе.
Задача состоит в том, чтобы вызвать метод &lt;tt class="docutils literal"&gt;doSomething()&lt;/tt&gt; у всех объектов
находящихся в контейнере.&lt;/p&gt;
&lt;p&gt;Первое, о чем подумают многие - это цикл:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="k"&gt;for&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;vector&lt;/span&gt;&lt;span class="o"&gt;&amp;lt;&lt;/span&gt;&lt;span class="n"&gt;Foo&lt;/span&gt;&lt;span class="o"&gt;&amp;gt;::&lt;/span&gt;&lt;span class="n"&gt;const_iterator&lt;/span&gt; &lt;span class="n"&gt;it&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;begin&lt;/span&gt;&lt;span class="p"&gt;();&lt;/span&gt; &lt;span class="n"&gt;it&lt;/span&gt; &lt;span class="o"&gt;!=&lt;/span&gt; &lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;end&lt;/span&gt;&lt;span class="p"&gt;();&lt;/span&gt; &lt;span class="o"&gt;++&lt;/span&gt;&lt;span class="n"&gt;it&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
    &lt;span class="n"&gt;it&lt;/span&gt;&lt;span class="o"&gt;-&amp;gt;&lt;/span&gt;&lt;span class="n"&gt;doSomething&lt;/span&gt;&lt;span class="p"&gt;();&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Но циклы — это не &amp;quot;кашерно&amp;quot;... Ведь мы знаем об алгоритме &lt;tt class="docutils literal"&gt;&lt;span class="pre"&gt;std::for_each&lt;/span&gt;&lt;/tt&gt; и
&lt;tt class="docutils literal"&gt;функциональных объектах&lt;/tt&gt;, которые, кстати говоря, хорошо оптимизируются
компиляторами, что позволяет писать более производительный код.&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="k"&gt;struct&lt;/span&gt; &lt;span class="n"&gt;DoMethod&lt;/span&gt; &lt;span class="o"&gt;:&lt;/span&gt; &lt;span class="k"&gt;public&lt;/span&gt; &lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;unary_function&lt;/span&gt;&lt;span class="o"&gt;&amp;lt;&lt;/span&gt;&lt;span class="n"&gt;Foo&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt;&lt;span class="kt"&gt;void&lt;/span&gt;&lt;span class="o"&gt;&amp;gt;&lt;/span&gt;
&lt;span class="p"&gt;{&lt;/span&gt;
    &lt;span class="kt"&gt;void&lt;/span&gt; &lt;span class="k"&gt;operator&lt;/span&gt;&lt;span class="p"&gt;()&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="k"&gt;const&lt;/span&gt; &lt;span class="n"&gt;Foo&lt;/span&gt;&lt;span class="o"&gt;&amp;amp;&lt;/span&gt; &lt;span class="n"&gt;foo&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt; &lt;span class="p"&gt;{&lt;/span&gt; &lt;span class="n"&gt;foo&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;doSomething&lt;/span&gt;&lt;span class="p"&gt;();&lt;/span&gt; &lt;span class="p"&gt;}&lt;/span&gt;
&lt;span class="p"&gt;};&lt;/span&gt;

&lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;for_each&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;begin&lt;/span&gt;&lt;span class="p"&gt;(),&lt;/span&gt; &lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;end&lt;/span&gt;&lt;span class="p"&gt;(),&lt;/span&gt; &lt;span class="n"&gt;DoMethod&lt;/span&gt;&lt;span class="p"&gt;());&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Конечно, часть из вас знает о новом стандарте &lt;strong&gt;C++0x&lt;/strong&gt; и о его лямбда-функциях.
Это позволит написать код короче, проще и, во многих случаях, понятней:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;for_each&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;begin&lt;/span&gt;&lt;span class="p"&gt;(),&lt;/span&gt; &lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;end&lt;/span&gt;&lt;span class="p"&gt;(),&lt;/span&gt; &lt;span class="p"&gt;[](&lt;/span&gt;&lt;span class="k"&gt;const&lt;/span&gt; &lt;span class="n"&gt;Foo&lt;/span&gt;&lt;span class="o"&gt;&amp;amp;&lt;/span&gt; &lt;span class="n"&gt;foo&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt; &lt;span class="p"&gt;{&lt;/span&gt;
    &lt;span class="n"&gt;foo&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;doSomething&lt;/span&gt;&lt;span class="p"&gt;();&lt;/span&gt;
&lt;span class="p"&gt;});&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;То, что я упомянул выше, вы, возможно, знали.. а может и не знали —
не имеет значение. Но то, что я покажу ниже, обычно не знают многие начинающие.
В &lt;em&gt;STL&lt;/em&gt; существует интересный способ, позволяющий указать имя метода, который
необходимо вызвать:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;for_each&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;begin&lt;/span&gt;&lt;span class="p"&gt;(),&lt;/span&gt; &lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;end&lt;/span&gt;&lt;span class="p"&gt;(),&lt;/span&gt; &lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;mem_fun_ref&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="o"&gt;&amp;amp;&lt;/span&gt;&lt;span class="n"&gt;Foo&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;doSomething&lt;/span&gt;&lt;span class="p"&gt;));&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Как по мне, это не менее читабельно приведенных выше способов. К тому же, не
менее производительно, чем при использовании функционального объекта.&lt;/p&gt;
&lt;p&gt;&lt;tt class="docutils literal"&gt;&lt;span class="pre"&gt;std::mem_fun_ref&lt;/span&gt;&lt;/tt&gt; является функцией-адаптером, которая создает и передает
в алгоритм &lt;tt class="docutils literal"&gt;&lt;span class="pre"&gt;std::for_each&lt;/span&gt;&lt;/tt&gt; функциональный объект.&lt;/p&gt;
&lt;p&gt;Я рекомендую использование именно этот способ. Почему? Да потому что он:&lt;/p&gt;
&lt;ul class="simple"&gt;
&lt;li&gt;нагляден - функциональные объекты частенько описываются в местах далеких от
применения;&lt;/li&gt;
&lt;li&gt;прост в написании;&lt;/li&gt;
&lt;li&gt;производительней цикла &lt;tt class="docutils literal"&gt;for&lt;/tt&gt;;&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Конечно, лямбда-функции тоже удовлетворяют указанным выше преимуществам.
Поэтому, их применение тоже приветствуется. Однако, стандарт еще не вышел, и
хотя современные компиляторы поддерживают его в той или иной мере, пройдет не
мало времени, пока он не станет корпоративным стандартом! :)&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;p.s:&lt;/strong&gt;
В посте я не упомянул еще один способ, который является альтернативой
циклу &lt;tt class="docutils literal"&gt;for&lt;/tt&gt;, указанному выше. Как известно, новый стандарт вводит понятие
&lt;tt class="docutils literal"&gt;&lt;span class="pre"&gt;range-based&lt;/span&gt; for&lt;/tt&gt; цикла. Его суть заключается в написании &lt;em&gt;for each&lt;/em&gt; цикла в
стиле Java, путем скрытого вызова методов &lt;tt class="docutils literal"&gt;begin()&lt;/tt&gt; и &lt;tt class="docutils literal"&gt;end()&lt;/tt&gt; у контейнера:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="k"&gt;for&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="k"&gt;const&lt;/span&gt; &lt;span class="n"&gt;Foo&lt;/span&gt;&lt;span class="o"&gt;&amp;amp;&lt;/span&gt; &lt;span class="n"&gt;foo&lt;/span&gt; &lt;span class="o"&gt;:&lt;/span&gt; &lt;span class="n"&gt;vec&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
    &lt;span class="n"&gt;foo&lt;/span&gt;&lt;span class="p"&gt;.&lt;/span&gt;&lt;span class="n"&gt;doSomething&lt;/span&gt;&lt;span class="p"&gt;();&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Достаточно полезное введение, которое повышает читаемость и простоту написания.
Вообще, новый стандарт очень удачен, но это уже другая история.&lt;/p&gt;
</summary><category term="cpp"></category><category term="algorithms"></category><category term="stl"></category></entry><entry><title>C++ стиль написания алгоритмов</title><link href="http://archive.kalnitsky.org/2011/04/16/writing-algorithms-in-cpp-style/" rel="alternate"></link><updated>2011-04-16T00:00:00+03:00</updated><author><name>Igor Kalnitsky</name></author><id>tag:archive.kalnitsky.org,2011-04-16:2011/04/16/writing-algorithms-in-cpp-style/</id><summary type="html">&lt;p&gt;В очередной раз не знаю с чего начать пост, поэтому скажу прямо. Мне хочеться
рассказать о правилах написания &lt;strong&gt;C++&lt;/strong&gt; кода. Нет, речь пойдет не о Style
Guide'ах, а о &lt;em&gt;«правильном»&lt;/em&gt; написании некоторых вещей.&lt;/p&gt;
&lt;p&gt;К сожалению, сложно объяснить, что я имею ввиду под словом &lt;em&gt;&amp;quot;правильный&amp;quot;&lt;/em&gt;. Среди
&lt;strong&gt;python&lt;/strong&gt; программистов широко распространено слово &lt;em&gt;pythonic&lt;/em&gt;, которое
означает, что код написан в стиле python (со всеми достоинствами этого языка),
а не, например, в стиле процедурных языков. То есть, это означает, что для
прохода по контейнерам используется цикл &lt;tt class="docutils literal"&gt;for each&lt;/tt&gt;, а не цикл с доступом по
индексу. Вот под словом &lt;em&gt;«правильный»&lt;/em&gt; я понимаю именно такой подход — подход
написания кода в стиле &lt;strong&gt;C++&lt;/strong&gt;.&lt;/p&gt;
&lt;p&gt;Для примера рассмотрим уже заезженную функцию бинарного поиска с рекурсивным
вызовом (не смотря на то, что бинарный поиск уже реализован в stl).
К примеру, перед программистом стала задача проверить вхождение некоторого
элемента в упорядоченный массив. Первое, что придет в голову начинающему
программисту — это решить проблему в лоб и написать что-то типа такого:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="kt"&gt;bool&lt;/span&gt; &lt;span class="nf"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="k"&gt;const&lt;/span&gt; &lt;span class="kt"&gt;int&lt;/span&gt;&lt;span class="o"&gt;*&lt;/span&gt; &lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="kt"&gt;int&lt;/span&gt; &lt;span class="n"&gt;size&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="kt"&gt;int&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
&lt;span class="p"&gt;{&lt;/span&gt;
    &lt;span class="c1"&gt;// element not found
&lt;/span&gt;    &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;size&lt;/span&gt; &lt;span class="o"&gt;==&lt;/span&gt; &lt;span class="mi"&gt;0&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
        &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nb"&gt;false&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
    &lt;span class="c1"&gt;// check for the desired element
&lt;/span&gt;    &lt;span class="kt"&gt;int&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;size&lt;/span&gt; &lt;span class="o"&gt;/&lt;/span&gt; &lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
    &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;key&lt;/span&gt; &lt;span class="o"&gt;==&lt;/span&gt; &lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;])&lt;/span&gt;
        &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nb"&gt;true&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
    &lt;span class="c1"&gt;// recursive call
&lt;/span&gt;    &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;key&lt;/span&gt; &lt;span class="o"&gt;&amp;lt;&lt;/span&gt; &lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;])&lt;/span&gt;
        &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="n"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
    &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="n"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;arr&lt;/span&gt; &lt;span class="o"&gt;+&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;+&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;size&lt;/span&gt; &lt;span class="o"&gt;-&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;-&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
&lt;span class="p"&gt;}&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Стоит отметить, что это лучший случай. Обычно передача указателя на массив
происходит без &lt;tt class="docutils literal"&gt;const&lt;/tt&gt;, а сигнатура функции может изобиловать дополнительными
аргументами.&lt;/p&gt;
&lt;p&gt;Более продвинутый новичок вспомнит о шаблонах и напишет следующий код:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="k"&gt;template&lt;/span&gt;&lt;span class="o"&gt;&amp;lt;&lt;/span&gt;&lt;span class="k"&gt;class&lt;/span&gt; &lt;span class="nc"&gt;T&lt;/span&gt;&lt;span class="o"&gt;&amp;gt;&lt;/span&gt;
    &lt;span class="kt"&gt;bool&lt;/span&gt; &lt;span class="n"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="k"&gt;const&lt;/span&gt; &lt;span class="n"&gt;T&lt;/span&gt;&lt;span class="o"&gt;*&lt;/span&gt; &lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="kt"&gt;int&lt;/span&gt; &lt;span class="n"&gt;size&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;T&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
    &lt;span class="p"&gt;{&lt;/span&gt;
        &lt;span class="c1"&gt;// element not found
&lt;/span&gt;        &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;size&lt;/span&gt; &lt;span class="o"&gt;==&lt;/span&gt; &lt;span class="mi"&gt;0&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
            &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nb"&gt;false&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
        &lt;span class="c1"&gt;// check for the desired element
&lt;/span&gt;        &lt;span class="kt"&gt;int&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;size&lt;/span&gt; &lt;span class="o"&gt;/&lt;/span&gt; &lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
        &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;key&lt;/span&gt; &lt;span class="o"&gt;==&lt;/span&gt; &lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;])&lt;/span&gt;
            &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nb"&gt;true&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
        &lt;span class="c1"&gt;// recursive call
&lt;/span&gt;        &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;key&lt;/span&gt; &lt;span class="o"&gt;&amp;lt;&lt;/span&gt; &lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;[&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;])&lt;/span&gt;
            &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="n"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;arr&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
        &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nf"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;arr&lt;/span&gt; &lt;span class="o"&gt;+&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;+&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;size&lt;/span&gt; &lt;span class="o"&gt;-&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;-&lt;/span&gt; &lt;span class="mi"&gt;1&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
    &lt;span class="p"&gt;}&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Ну что же, уже неплохою Во всяком случае, функция будет работать для
разнообразных массивов, элементами которого служат типы с определенными
операторами сравнения &lt;tt class="docutils literal"&gt;==&lt;/tt&gt; и &lt;tt class="docutils literal"&gt;&amp;lt;&lt;/tt&gt;.&lt;/p&gt;
&lt;p&gt;Часто подобная реализации пишется не для массива, а для некоторого контейнера:
программист считает себя крутым, передает контейнер и ему уже не нужно
передавать размер. Несмотря на то, что тип контейнера будет шаблонным, зачастую
в коде все равно существует привязка к типу контейнера. Это либо индексный
доступ, либо использование &lt;tt class="docutils literal"&gt;push_front&lt;/tt&gt;/&lt;tt class="docutils literal"&gt;push_back&lt;/tt&gt;/&lt;tt class="docutils literal"&gt;etc&lt;/tt&gt;. Специфичных
функций-членов класса у каждого контейнера хватает.&lt;/p&gt;
&lt;p&gt;Но такой подход я не назвал бы правильным с точки зрения C++. Этот язык куда
более гибкий, мощный и предоставляет широкие средства для написания кода.
Поэтому, подобные алгоритмы (алгоритмы, работающие с некоторыми диапазонами:
массивы, контейнеры) лучше реализовывать в стиле STL: с аргументами
в виде итераторов, задающих последовательность. Выглядеть это
будет примерно так:&lt;/p&gt;
&lt;pre class="code c++ literal-block"&gt;
&lt;span class="k"&gt;template&lt;/span&gt;&lt;span class="o"&gt;&amp;lt;&lt;/span&gt;&lt;span class="k"&gt;class&lt;/span&gt; &lt;span class="nc"&gt;IT&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="k"&gt;class&lt;/span&gt; &lt;span class="nc"&gt;KEY&lt;/span&gt;&lt;span class="o"&gt;&amp;gt;&lt;/span&gt;
    &lt;span class="kt"&gt;bool&lt;/span&gt; &lt;span class="n"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;IT&lt;/span&gt; &lt;span class="n"&gt;first&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;IT&lt;/span&gt; &lt;span class="n"&gt;last&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;KEY&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
    &lt;span class="p"&gt;{&lt;/span&gt;
        &lt;span class="c1"&gt;// element not found
&lt;/span&gt;        &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;first&lt;/span&gt; &lt;span class="o"&gt;==&lt;/span&gt; &lt;span class="n"&gt;last&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
            &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nb"&gt;false&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
        &lt;span class="c1"&gt;// check for the desired element
&lt;/span&gt;        &lt;span class="n"&gt;IT&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt; &lt;span class="o"&gt;=&lt;/span&gt; &lt;span class="n"&gt;first&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
        &lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;advance&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;std&lt;/span&gt;&lt;span class="o"&gt;::&lt;/span&gt;&lt;span class="n"&gt;distance&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;first&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;last&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt; &lt;span class="o"&gt;/&lt;/span&gt; &lt;span class="mi"&gt;2&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
        &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;key&lt;/span&gt; &lt;span class="o"&gt;==&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
            &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nb"&gt;true&lt;/span&gt;&lt;span class="p"&gt;;&lt;/span&gt;
        &lt;span class="c1"&gt;// recursive call
&lt;/span&gt;        &lt;span class="k"&gt;if&lt;/span&gt; &lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;key&lt;/span&gt; &lt;span class="o"&gt;&amp;lt;&lt;/span&gt; &lt;span class="o"&gt;*&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;)&lt;/span&gt;
            &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="n"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="n"&gt;first&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
        &lt;span class="k"&gt;return&lt;/span&gt; &lt;span class="nf"&gt;binary_search&lt;/span&gt;&lt;span class="p"&gt;(&lt;/span&gt;&lt;span class="o"&gt;++&lt;/span&gt;&lt;span class="n"&gt;half&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;last&lt;/span&gt;&lt;span class="p"&gt;,&lt;/span&gt; &lt;span class="n"&gt;key&lt;/span&gt;&lt;span class="p"&gt;);&lt;/span&gt;
    &lt;span class="p"&gt;}&lt;/span&gt;
&lt;/pre&gt;
&lt;p&gt;Казалось бы - всё отлично! Функция работает как для массива, так и для вектора,
листа, множества и других контейнеров :) Но, как известно, нет предела
совершенству. Данную функцию можно усовершенствовать тем, что передавать
предикаты. Например, предикат упорядочивания - сравнивающий два элемента.
Таким образом, бинарный поиск будет применим к последовательностям упорядоченным
не по оператору &lt;strong&gt;&amp;lt;&lt;/strong&gt;, а по некоторому предикату (что в реальных проектах
встречается весьма не редко). Но и текущего варианта будет вполне достаточно для
того, чтобы считать код хорошим и правильным с точки зрения философии C++. :)&lt;/p&gt;
&lt;p&gt;Что отсюда нужно вынести? А нужно вынести следующее:&lt;/p&gt;
&lt;ul class="simple"&gt;
&lt;li&gt;если алгоритм обобщенный, писать для него шаблон;&lt;/li&gt;
&lt;li&gt;последовательность задавать итераторами, а не передачей контейнера или массива;&lt;/li&gt;
&lt;li&gt;последовательность должна задаваться началом и элементом, следующим за
последним [first, last);&lt;/li&gt;
&lt;li&gt;использовать функции STL для работы с итераторами, дабы обеспечить
максимальную переносимость алгоритма (например &lt;tt class="docutils literal"&gt;advance&lt;/tt&gt;/&lt;tt class="docutils literal"&gt;distance&lt;/tt&gt;);&lt;/li&gt;
&lt;li&gt;вообще максимально возможно использовать STL, а не придумывать велосипеды!&lt;/li&gt;
&lt;/ul&gt;
</summary><category term="cpp"></category><category term="algorithms"></category><category term="coding_style"></category></entry></feed>